import { UserRegister } from './../models/user-register';
import { RegisterService } from './../services/auth/register.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup , Validators} from '@angular/forms';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  formRegister: FormGroup;

  constructor(private formBuilder: FormBuilder, private registerService: RegisterService) { }

  ngOnInit(): void {
    this.formRegister = this.formBuilder.group({
      firstname: [null, Validators.required],
      lastname: [null, [Validators.required]],
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      passwordConfirmation: [null, [Validators.required]], 
      email:[null, [Validators.required,Validators.email]],
      birthday:[null, [Validators.required]]
    });
  }
  
  checkPassword(group: FormGroup): boolean {
    let pass = group.get('password').value;
    let confirmPass = group.get('passwordConfirmation').value;
    return pass === confirmPass;
  }

  register(){
    console.error(this.formRegister);
    if (this.formRegister?.valid) {
      const userRegister: UserRegister = {
        firstName: this.formRegister.value?.firstname,
        lastName: this.formRegister.value?.lastname,
        password: this.formRegister.value?.password,
        username: this.formRegister.value?.username,
        email: this.formRegister.value?.email,
        birthday: this.formRegister.value?.birthday,
      }
      console.error(userRegister);
      this.registerService.register(userRegister);
    } else {
      console.error('error register');
    }
  }

}
