import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'esFront';

  /**
   * Function to check if the is authentificted
   */
  isAuthentificated(): boolean {
    return localStorage.getItem('token') != undefined;
  }
}
