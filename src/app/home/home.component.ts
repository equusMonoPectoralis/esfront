import { ProductService } from './../services/product/product.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
  }

  logout(): void {
    localStorage.removeItem('token');
  }

  insertBook() {
    this.productService.insertBook();
  }

}
