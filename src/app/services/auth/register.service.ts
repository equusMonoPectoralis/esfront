import { ResponsePayload } from './../../models/response-payload';
import { UserRegister } from './../../models/user-register';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private httpClient: HttpClient) { }

  register(user: UserRegister){
    const url = environment.apiUrl + '/auth/signup';
    this.httpClient.post(url, user).subscribe(
      (result: ResponsePayload) => {
        if(result.success){
          console.error(result);
        } else{
          console.error("error while creating user");  
        }       
      },
      error => {
        console.error(error);
      }
    )
  }
}
