export interface Book {
    id: string;
    author: string;
    name: string;
}
