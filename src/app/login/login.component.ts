import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Auth } from '../models/auth-model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService) { }

  ngOnInit(): void {
    this.formLogin = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, [Validators.required, this.checkPassword]],
    });
  }

  checkPassword(): boolean {
    return true;
  }

  /**
   * User login
   */
  login() {
    if (this.formLogin?.valid) {
      console.log('formLogin: ', this.formLogin);
      const userAuth: Auth = {
        username: this.formLogin.value?.username,
        password: this.formLogin.value?.password
      }
      this.authService.login(userAuth);
    } else {
      console.error('error formLogin');
    }
  }

}
